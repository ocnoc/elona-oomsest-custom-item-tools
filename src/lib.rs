use std::io::prelude::*;
use std::io;
use std::env;
use std::fs;
use std::path::Path;
use std::char;
use std::str;
use flate2::read::GzDecoder;
use flate2::write::GzEncoder;
use flate2::Compression;

struct ItemData {
    data: Vec<u8>
}

#[derive(std::fmt::Debug)]
pub struct CustomPartFile {
    pub name: String,
    pub data: Vec<u8>,
}

impl CustomPartFile {
    pub fn new(name: &str, data: Vec<u8>) -> CustomPartFile {
        return CustomPartFile {
            name: name.clone().to_string(),
            data
        }
    }

    pub fn decompress(&mut self) {
        self.data = decode_data(&self.data);
    }

    pub fn compress(&mut self) {
        self.data = encode_data(&self.data);
    }
}

// Named unzip2 since that's what the elona function is named
pub fn unzip2(filename: &String) -> Vec<CustomPartFile> {
    let mut return_values: Vec<CustomPartFile> = Vec::new();
    // values are Header size, size of file name, size of file length
    let values:Vec<usize> = vec!(50,40,10);
    let path = Path::new(filename);
    let mut file = fs::File::open(path).expect("Error occurred opening file");
    let mut file_contents: Vec<u8> = Vec::new();
    file.read_to_end(&mut file_contents);
    let mut length_head = 0;
    while length_head < file_contents.len() {
        let file_name: &[u8] = &file_contents[length_head..length_head+values[1]];
        let file_length: &[u8] = &file_contents[length_head+values[1]..length_head+values[1]+values[2]];
        let mut temp_string = String::new();
        for i in file_length.iter() {
            if i.clone() != 0 as u8 {
                temp_string.push(char::from_u32(i.clone() as u32).unwrap());
            }
        }
        let length: u32 = temp_string.parse().unwrap();
        if length == 0 {
            break;
        }
        let start = length_head as usize+values[0];
        let end = start+(length as usize-(start-length_head)) as usize;
        let filetemp = &file_contents[start..end];
        let mut file_name = str::from_utf8(&file_name).unwrap();
        file_name = file_name.trim_end_matches(char::from_u32(0).unwrap());
        let item_part_file = CustomPartFile::new(file_name, filetemp.to_vec());
        return_values.push(item_part_file);
        length_head += length as usize;
    }
    return return_values
}

pub fn zip2(data: Vec<CustomPartFile>) -> Vec<u8> {
    let mut packed_data:Vec<u8> = Vec::new();
    let mut lensum = 0;
    for item_part in data.iter() {
        let name = item_part.name.as_bytes();
        let values = vec![50,40,10];
        let data = &item_part.data;
        let length = data.len() + values[0];
        let lenhead = lensum;
        lensum += length;
        let mut name_header: Vec<u8> = Vec::new();
        let mut length_header: Vec<u8> = Vec::new();
        name_header.extend_from_slice(name);
        name_header.resize(values[1], 0);
        let str_len = length.to_string();
        length_header.extend_from_slice(str_len.as_bytes());
        length_header.resize(values[2], 0);
        let mut filebuffer: Vec<u8> = Vec::new();
        filebuffer.extend(name_header.iter());
        filebuffer.extend(length_header.iter());
        filebuffer.extend(data.iter());
        packed_data.extend(filebuffer.iter());
    }
    return packed_data;
}

// Decodes a vector of bytes using
fn decode_data(data: &Vec<u8>) -> Vec<u8> {
    let mut decoder = GzDecoder::new(&data[..]);
    let mut decompressed_data = Vec::new();
    decoder.read_to_end(&mut decompressed_data);
    return if decompressed_data.len() > 0 { decompressed_data } else { data.to_vec() };
}

fn encode_data(data: &Vec<u8>) -> Vec<u8> {
    let mut encoder = GzEncoder::new(Vec::new(), Compression::default());
    encoder.write_all(&data[..]).unwrap();
    return encoder.finish().unwrap();
}

