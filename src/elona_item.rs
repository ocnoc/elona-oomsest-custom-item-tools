use std::collections::HashMap;
use linked_hash_map::LinkedHashMap;
use encoding_rs::SHIFT_JIS;
use std::error::Error;

pub struct FixEncInvoke {
    pub identifier: u32,
    pub target: u32,
    pub chance: u32, // may be uselless
    pub chance_melee: u32, // useless
    pub chance_ranged: u32, // useless
    pub invoke_chance: u32
}

pub struct ItemDefinition {
    pub ename: Option<String>,
    pub jname: Option<String>,
    pub ialphanameref: Option<String>,
    pub ifilterref: Option<String>,
    pub iknownnameref: Option<String>,
    pub author: Option<String>,
    pub isnd: Option<String>,
    pub iorgvalue: Option<u32>,
    pub iorgweight: Option<u32>,
    pub dicex: Option<u32>,
    pub dicey: Option<u32>,
    pub fixdamage: Option<u32>,
    pub fixhit: Option<u32>,
    pub dodgevalue: Option<u32>,
    pub protectvalue: Option<u32>,
    pub relaskill: Option<u32>,
    pub material: Option<u32>,
    pub identifydef: Option<u32>,
    pub ilight: Option<u32>,
    pub reftype: Option<u32>,
    pub reftypeminor: Option<u32>,
    pub ibitvaluable: Option<u32>,
    pub objlv: Option<u32>,
    pub fixlv: Option<u32>,
    pub givegod: Option<u32>,
    pub fixenc: HashMap<u32, u32>,
    pub fixencinvoke: HashMap<u32,FixEncInvoke>,
    pub icolref: Option<u32>,
    pub isetpos: Option<u32>,
    pub ipilepos: Option<u32>,
    pub idropshadow: Option<u32>,
    pub irare: Option<u32>,
    pub ipierce: Option<u32>,
    pub irangepow: Option<Vec<u32>>,
    pub irangehit: Option<Vec<u32>>,
    pub inammeonly: Option<u32>,
    pub ieffect: Option<u32>,
    pub cargoitem: Option<u32>,
    pub isub: Vec<u32>,
    pub ispawntype: Option<u32>,
    pub txtdescription: Vec<String>
}

impl ItemDefinition {
    pub fn new() -> ItemDefinition {
        ItemDefinition {
            ename: None,
            jname: None,
            ialphanameref: None,
            ifilterref: None,
            iknownnameref: None,
            author: None,
            isnd: None,
            iorgvalue: None,
            iorgweight: None,
            dicex: None,
            dicey: None,
            fixdamage: None,
            fixhit: None,
            dodgevalue: None,
            protectvalue: None,
            relaskill: None,
            material: None,
            identifydef: None,
            ilight: None,
            reftype: None,
            reftypeminor: None,
            ibitvaluable: None,
            objlv: None,
            fixlv: None,
            givegod: None,
            fixenc: Default::default(),
            fixencinvoke: Default::default(),
            icolref: None,
            isetpos: None,
            ipilepos: None,
            idropshadow: None,
            irare: None,
            ipierce: None,
            irangepow: None,
            irangehit: None,
            inammeonly: None,
            ieffect: None,
            cargoitem: None,
            isub: vec![],
            ispawntype: None,
            txtdescription: vec![]
        }
    }
}

pub fn parse_elona_txt_format(data: &String) -> LinkedHashMap<String, Vec<String>> {
    let mut tag_to_data_map: LinkedHashMap<String, Vec<String>> = LinkedHashMap::new();
    let mut data = data.to_owned();
    data = data.replace('\r', "");
    let split_data: Vec<_> = data.split("\n").collect();
    let mut first = true;
    let mut iterator = split_data.iter().peekable();
    while let Some(line) = iterator.next() {
        if first {
            first = false;
            continue;

        }
        if line.contains(".") {
            let split: Vec<_> = line.split(".").collect();
            let (tag, data) = (split[0], split[1].trim_start());
            let data = data.trim_start_matches('\"');
            let data = data.trim_end_matches('\"');
            let data_vector = data.split(",").map(|element| element.to_owned()).collect::<Vec<_>>();
            tag_to_data_map.insert(tag.to_owned(), data_vector);
            continue;
        }
        if line.starts_with("%") && line.contains(",") {
            let tags = line.split(",").collect::<Vec<_>>();
            let tag = tags[0];
            let mut text: Vec<String> = Vec::new();
            text.push(tags[1].to_string());
            while let Some(next) = iterator.peek() {
                if (next.starts_with('%') && !next.eq(&&"%END")) || next.eq(&&"") {
                    break;
                }
                let next = iterator.next().unwrap().to_string();
                text.push(next);
            }
            tag_to_data_map.insert(tag.to_owned(), text);
            continue;
        }
    }
    return tag_to_data_map;
}

pub fn output_elona_txt(map: &LinkedHashMap<String, Vec<String>>, header: &String) -> String {
    let line_ending = "\r\n";
    let file_footer = "%endTxt";
    let text_indicator = "%";
    let mut result = String::new();
    result.push_str(&header[..]);
    result.push_str("\r\n");
    for (tag, data) in map {
        if tag.starts_with(text_indicator) {
            let mut new_tag = tag.to_owned() + ",";
            new_tag.push_str(&data[0]);
            new_tag.push_str(line_ending);
            result.push_str(&new_tag[..]);
            let mut first = true;
            for line in data.iter() {
                if first {
                    first = false;
                    continue;
                }
                result.push_str(&line[..]);
                result.push_str(line_ending);
            }
            if data.len() == 1 {
                result.push_str(line_ending);
            }
            continue;
        }
        let mut new_tag: String = tag.to_owned() + ".    ";
        let mut result_string = data.join(",");
        result_string.insert(0, '\"');
        result_string.push_str("\"");
        result_string.push_str(line_ending);
        new_tag.push_str(&result_string[..]);
        result.push_str(&new_tag[..]);
    }
    result.push_str(file_footer);
    return result;
}

pub fn decode_sjis(data: &Vec<u8>) -> Result<String, &'static str> {
    let (data_string, _enc, errors) = SHIFT_JIS.decode(data);
    if errors {
        return Err("error occurred in decoding sjis data");
    }
    return Ok(data_string.into_owned());
}

pub fn encode_sjis(string: &String) -> Result<Vec<u8>, &'static str> {
    let (result_data, _enc, errors) = SHIFT_JIS.encode(string.as_str());
    if errors {
        return Err("error encoding string into sjis");
    }
    return Ok(result_data.to_vec());
}