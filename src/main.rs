extern crate encoding_rs;

use std::io::prelude::*;
use std::io;
use std::env;
use std::fs;
use std::path::Path;
use std::char;
use std::str;
use flate2::read::GzDecoder;
use elona_oomsest_item_extractor::{unzip2, CustomPartFile, zip2};
use core::borrow::Borrow;
use byteorder::{LittleEndian,WriteBytesExt};
use std::fs::DirEntry;
use std::collections::HashMap;
use std::env::set_current_dir;
use elona_item::{parse_elona_txt_format, output_elona_txt, encode_sjis, decode_sjis};
use encoding_rs::SHIFT_JIS;

mod elona_item;
mod application;

fn main() {
    let args: Vec<String> = env::args().collect();
    if args.len() == 1 {
        application::start_application();
    }
    let mode = &args[1];
    //println!("Mode: {:?}", mode);
    if mode.eq(&"d".to_string()) {
        let item_file = &args[2];
        let ext = Path::new(item_file).extension().unwrap().to_str().unwrap().to_string();
        decompress_item_file(item_file, get_part_extension_mapping(&ext));
    }
    if mode.eq(&"c".to_string()) {
        let outfile = &args[2];
        let files_to_zip = &args[3..].to_vec();
        let mut god = false;
        if outfile.ends_with(".god") {
            god = true;
        }
        compress_item_file(files_to_zip, outfile, god);
    }
    if mode.eq(&"bd".to_string()) {
        let directory = &args[2];
        decompress_directory(directory);
    }
}

fn decompress_item_file(item_file_name: &String, extension_mapping: HashMap<String, String>) {
    let out_dir = Path::new(item_file_name).file_stem().unwrap().to_str().unwrap();
    let item_name = Path::new(item_file_name).file_name().unwrap().to_str().unwrap();
    if !Path::new(out_dir).exists() {
        fs::create_dir(Path::new(out_dir));
    }
    let mut extension = &"".to_owned();
    let out_dir_path = Path::new(out_dir);
    let mut item_parts = unzip2(item_file_name);
    for item_part in item_parts.iter_mut() {
        item_part.name = Path::new(&item_part.name).file_stem().unwrap().to_str().unwrap().to_string();
        if let Some(e) = extension_mapping.get(&item_part.name) {
            item_part.name.push_str(".");
            item_part.name.push_str(e);
            extension = e;
        }
        item_part.decompress();
        let mut outfile = fs::File::create(out_dir_path.join(Path::new(&item_part.name))).unwrap();
        outfile.write_all(&item_part.data[..]);
        // If we unpacked a file with an extension we know, unpack that one too
        if get_compressed_types().contains(&extension) {
            let current_dir = env::current_dir().unwrap();
            set_current_dir(out_dir_path);
            decompress_item_file(&item_part.name, get_part_extension_mapping(extension));
            set_current_dir(current_dir);
        }
    }
}
// Gods are weird with the god1.t file, they skip the first block and do the god2.txt length in the second, so we need to handle it
fn compress_item_file(files: &Vec<String>, output: &String, is_god: bool) {
    let length_header_buffer_size = 75*4; // magic number that sest uses
    let mut parts: Vec<CustomPartFile> = Vec::new();
    let outfile_path = Path::new(output);
    for file_name in files.iter() {
        let mut buffer: Vec<u8> = Vec::new();
        let mut file = fs::File::open(Path::new(file_name)).unwrap();
        file.read_to_end(&mut buffer);
        let stem = Path::new(file_name).file_stem().unwrap().to_str().unwrap().to_owned();
        let t_file_name = stem+".t";
        let file_part = CustomPartFile::new(&t_file_name, buffer);
        parts.push(file_part);
    }
    let mut length_header_buffer: Vec<u8> = Vec::new();
    if is_god {
        length_header_buffer.write_u32::<LittleEndian>(0).unwrap();
        let len = parts[0].data.len();
        length_header_buffer.write_u32::<LittleEndian>(len as u32).unwrap();
    }
    else {
        for part in parts.iter() {
            let len = &(part.data).len();
            length_header_buffer.write_u32::<LittleEndian>(len.to_owned() as u32).unwrap();
        }
    }
    length_header_buffer.resize(length_header_buffer_size, 0);
    let length_header_name = outfile_path.extension().unwrap().to_str().unwrap().to_string() + "1.t";
    let length_header = CustomPartFile::new(&length_header_name, length_header_buffer);
    parts.insert(0, length_header);
    if !is_god {
        for part in parts.iter_mut() {
            part.compress();
        }
    }
    else {
        parts[0].compress();
        parts[1].compress();
    }
    let zipped_file = zip2(parts);
    let mut outfile = fs::File::create(outfile_path).unwrap();
    outfile.write_all(&zipped_file[..]).unwrap();
}

fn decompress_directory(directory: &String) {
    let dir = Path::new(directory);
    if !dir.is_dir() {
        println!("Please specify a directory for bulk decompression");
        return;
    }
    let files_iter = fs::read_dir(dir).unwrap().filter(|f| !f.as_ref().unwrap().path().is_dir());
    let files: Vec<_> = files_iter.collect();
    for file in files.iter() {
        let file = file.as_ref().unwrap();
        let file_path = file.path();
        if let Some(e) = file_path.extension() {
            match e.to_str().unwrap() {
                "item" | "ooitem" | "god" | "npc" =>  decompress_item_file(&String::from(file_path.to_str().unwrap()), get_part_extension_mapping(&e.to_str().unwrap().to_string())),
                _ => continue,
            }
        }
    }
}

fn get_part_extension_mapping(ext: &String) -> HashMap<String, String> {
    let mut result: HashMap<String, String> = HashMap::new();
    if ext.eq(&"item".to_string()) || ext.eq(&"ooitem".to_string()) {
        result.insert("item2".to_owned(), "bmp".to_owned());
        result.insert("item3".to_owned(), "txt".to_owned());
    }
    if ext.eq(&"npc".to_string()) {
        result.insert("npc2".to_owned(), "bmp".to_owned());
        result.insert("npc3".to_owned(), "txt".to_owned());
        result.insert("npc4".to_owned(), "bmp".to_owned());
    }
    if ext.eq(&"god".to_string()) {
        result.insert("god2".to_owned(), "txt".to_owned());
        result.insert("servant".to_owned(), "npc".to_owned());
        result.insert("gem".to_owned(), "item".to_owned());
        result.insert("artifact".to_owned(), "item".to_owned());
        result.insert("branch".to_owned(), "npc".to_owned());
        result.insert("ground".to_owned(), "eum".to_owned());
    }
    return result;
}

fn get_compressed_types() -> Vec<String> {
    return vec!["npc".to_owned(), "item".to_owned(), "ooitem".to_owned(), "god".to_owned()];
}