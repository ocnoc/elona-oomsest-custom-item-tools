use crate::elona_item::ItemDefinition;
extern crate gio;
extern crate gtk;
use gio::prelude::*;
use gtk::prelude::*;

use std::env::args;

pub fn start_application() {
    gtk::init().unwrap_or_else(|err| panic!("GTK could not initialize : {}", err));
    let glade_src = include_str!("application.glade");
    let builder = gtk::Builder::new_from_string(glade_src);
    let window: gtk::Window = builder.get_object("window1").unwrap();
    let exit_button: gtk::MenuItem = builder.get_object("exit_button").unwrap();
    exit_button.connect_activate(|_| gtk::main_quit());
    window.connect_destroy(|_| gtk::main_quit());
    window.show_all();
    gtk::main();
}