An in progress set of tools for working with Elona omake custom files. Should work with every omake variant but designed with oomsest in mind.

Can extract an item, god or cnpc into it's respective parts (txt, bmp, etc) by running the program as follows:
```
./elona-oomsest-item-extractor d this_is_an_item.item
```
If you're on windows, you'd probably run the exe through command line with the same arguments.

The extraction will result in 3 files appearing: item1, item2.bmp, item3.txt
item1 is ignorable, it's used by the game to set some buffer lengths. item2 is the picture used, item3 is the textfile data the item was made with.

You can decompile npcs and gods the same way. Npcs will turn into 3 or 4 files depending on if they have a face file, gods will become many files and folders.

You can compile an item/npc/god file as follows:
```
./elona-oomsest-item-extractor c item2.bmp item3.txt
```

The files should be named exactly as you get them out of an extraction. They should also be in the correct order, in increasing number, not including 1. Not sure about gods yet, 
i'll update this when I know.

You can batch decompile a directory as follows:
```
./elona-oomsest-item-extractor bd directory
```

This will go through every valid file in a directory (npc, god and item) and extract them to their own folders

Some notes:
The game compresses the data in each part using gzip, then uses it's own zipping algorithm to add the compressed data to the final file (ie .item). The frontend here (main) handles the preparation of the data to pass to those functions, and should be the only part you need to touch for expanding to other custom files right now.

You do not need to use the compile function. Elona will work fine if you follow the create a new item steps again with the extracted files. It's just something that may make rapid prototyping easier.

There's no error checking right now really, and once it gets added there won't be any consistency checking either for a bit. The program expects that what you feed it is correct, and never looks at the data itself, so it doesn't know anything about the items. Compiling invalid items and adding them to your game will make it crash.